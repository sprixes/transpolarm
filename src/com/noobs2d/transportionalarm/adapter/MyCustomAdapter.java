/**
 * 
 */

package com.noobs2d.transportionalarm.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.noobs2d.transportionalarm.R;
import com.noobs2d.transportionalarm.activity.Map;
import com.noobs2d.transportionalarm.database.AlarmLocationDataSource;
import com.noobs2d.transportionalarm.interfaces.AdapterListener;
import com.noobs2d.transportionalarm.model.AlarmLocationModel;

/**
 * Copyright (C) 2013 Arjay Paulino 
 * Company <http://www.cybersoft.com> 
 * email<arjay.paulino@cybersoft.ph>
 *
 */
/** @author JR */
public class MyCustomAdapter extends ArrayAdapter<AlarmLocationModel> {

//
	private List<AlarmLocationModel> alarmLocationModels;
	private AdapterListener adapterListener;

	public List<AlarmLocationModel> getAlarmLocationModels () {
		return alarmLocationModels;
	}

	public void setAlarmLocationModels (List<AlarmLocationModel> alarmLocationModels) {
		this.alarmLocationModels = alarmLocationModels;
	}

	public void setAdapterLister (AdapterListener adapterListener) {
		this.adapterListener = adapterListener;
	}

	private Context context;

	public MyCustomAdapter (Context context, int textViewResourceId, List<AlarmLocationModel> alarmLocationModels) {
		super(context, textViewResourceId, alarmLocationModels);
		this.context = context;
		this.alarmLocationModels = new ArrayList<AlarmLocationModel>();
		this.alarmLocationModels.addAll(alarmLocationModels);
	}

	private class ViewHolder {
		TextView alarmLocationName;
		TextView alarmLocationAddress;
		TextView alarmLocationLongitude;
		TextView alarmLocationLatitude;
		TextView alarmLocationArea;
		Button alarmLocationEditImageView;
		Button alarmLocationRemoveImageView;
		CheckBox alarmLocationCheckBox;
		int alarmLocaionId;

	}

	@Override
	public View getView (int position, View convertView, final ViewGroup parent) {

		ViewHolder holder = null;

		Log.v("ConvertView", String.valueOf(position));

		if (convertView == null) {

			LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = vi.inflate(R.layout.state_info, null);

			holder = new ViewHolder();
			holder.alarmLocationName = (TextView)convertView.findViewById(R.id.locationNameTextView);
			holder.alarmLocationAddress = (TextView)convertView.findViewById(R.id.locationAddressTextView);
			holder.alarmLocationLongitude = (TextView)convertView.findViewById(R.id.locationLongitudeTextView);
			holder.alarmLocationLatitude = (TextView)convertView.findViewById(R.id.locationLatitudetextView);
			holder.alarmLocationArea = (TextView)convertView.findViewById(R.id.locationAreaTextView);
			holder.alarmLocationEditImageView = (Button)convertView.findViewById(R.id.editImageView);
			holder.alarmLocationRemoveImageView = (Button)convertView.findViewById(R.id.addLocationImageView);
			holder.alarmLocationCheckBox = (CheckBox)convertView.findViewById(R.id.statusCheckBox);
			holder.alarmLocaionId = 0;
			convertView.setTag(holder);
			holder.alarmLocationCheckBox.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick (View v) {
					CheckBox checkBox = (CheckBox)v;
					AlarmLocationModel mobileListModel = (AlarmLocationModel)checkBox.getTag();
					AlarmLocationDataSource alarmLocationDataSource = new AlarmLocationDataSource(context);
					alarmLocationDataSource.open();
					alarmLocationDataSource.updateAllAlarmLocation();
					if (checkBox.isChecked()) {
						mobileListModel.setStatus("1");
					} else {
						mobileListModel.setStatus("0");
					}
					alarmLocationDataSource.updateAlarmLocation(mobileListModel);
					alarmLocationDataSource.close();
					adapterListener.onUpdateAdapter();
				}

			});

			holder.alarmLocationEditImageView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick (View v) {
					Button editView = (Button)v;
					AlarmLocationModel mobileListModel = (AlarmLocationModel)editView.getTag();
					Intent intent = new Intent(context, Map.class);
					intent.putExtra("AlarmLocationModel", mobileListModel);
					context.startActivity(intent);
					Toast.makeText(context, "Edit: " + mobileListModel.getName(), Toast.LENGTH_LONG).show();

				}
			});
			holder.alarmLocationRemoveImageView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick (View v) {
					Button editView = (Button)v;
					AlarmLocationModel mobileListModel = (AlarmLocationModel)editView.getTag();
					onDeleteConfirmDialog(mobileListModel).show();
					Toast.makeText(context, "Remove: " + mobileListModel.getName(), Toast.LENGTH_LONG).show();

				}
			});

		} else {
			holder = (ViewHolder)convertView.getTag();
		}

		AlarmLocationModel alarmLocationModel = alarmLocationModels.get(position);
		holder.alarmLocationName.setText("Name: " + alarmLocationModel.getName());
		holder.alarmLocationAddress.setText("Address: " + alarmLocationModel.getAddress());
		holder.alarmLocationLongitude.setText("Longitude: " + alarmLocationModel.getLongitude());
		holder.alarmLocationLatitude.setText("Latitude: " + alarmLocationModel.getLatitude());
		holder.alarmLocationArea.setText("Area: " + alarmLocationModel.getArea());
		if (alarmLocationModel.getStatus().equals("0")) {
			holder.alarmLocationCheckBox.setChecked(false);
		} else {
			holder.alarmLocationCheckBox.setChecked(true);
		}

		holder.alarmLocationEditImageView.setTag(alarmLocationModel);
		holder.alarmLocationRemoveImageView.setTag(alarmLocationModel);
		holder.alarmLocationCheckBox.setTag(alarmLocationModel);
		holder.alarmLocaionId = alarmLocationModel.getId();

		return convertView;
	}

	public Dialog onDeleteConfirmDialog (final AlarmLocationModel alarmLocationModel) {
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage("This action will delete the selected Location. Are you sure?")
			.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				@Override
				public void onClick (DialogInterface dialog, int id) {
					AlarmLocationDataSource alarmLocationDataSource = new AlarmLocationDataSource(context);
					alarmLocationDataSource.open();
					alarmLocationDataSource.deleteAlarmLocation(alarmLocationModel);
					alarmLocationDataSource.close();
					adapterListener.onUpdateAdapter();
				}
			}).setNegativeButton("NO", new DialogInterface.OnClickListener() {
				@Override
				public void onClick (DialogInterface dialog, int id) {
					// User cancelled the dialog
				}
			});
		// Create the AlertDialog object and return it
		return builder.create();
	}

}
