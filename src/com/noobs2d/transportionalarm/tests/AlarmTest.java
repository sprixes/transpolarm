
package com.noobs2d.transportionalarm.tests;

import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.test.ViewAsserts;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;

import com.noobs2d.transportionalarm.R;
import com.noobs2d.transportionalarm.activity.Alarm;

public class AlarmTest extends ActivityInstrumentationTestCase2<Alarm> {
	private Alarm mAlarm;
	private Button mStopButton;

	public AlarmTest () {
		super(Alarm.class);

	}

	@Override
	protected void setUp () throws Exception {
		super.setUp();

		// Sets the initial touch mode for the Activity under test. This must be called before
		// getActivity()
		setActivityInitialTouchMode(true);
		// Get a reference to the Activity under test, starting it if necessary.
		mAlarm = getActivity();
		mStopButton = (Button)mAlarm.findViewById(R.id.StopAlarm);
	}

	@MediumTest
	public void testStopButton_layout () {
		// Retrieve the top-level window decor view
		final View decorView = mAlarm.getWindow().getDecorView();

		// Verify that the mClickMeButton is on screen
		ViewAsserts.assertOnScreen(decorView, mStopButton);

		// Verify width and heights
		final ViewGroup.LayoutParams layoutParams = mStopButton.getLayoutParams();
		assertNotNull(layoutParams);
		TouchUtils.clickView(this, mStopButton);
	}

	/** Tests the preconditions of this test fixture. */
	@MediumTest
	public void testStopButtonEvent () {
		assertNotNull("mStopButton is null", mStopButton);
		// mStopButton.performClick();
		TouchUtils.clickView(this, mStopButton);
	}

}
