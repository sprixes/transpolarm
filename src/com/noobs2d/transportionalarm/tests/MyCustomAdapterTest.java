
package com.noobs2d.transportionalarm.tests;

import java.util.ArrayList;

import android.test.AndroidTestCase;
import android.test.TouchUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.noobs2d.transportionalarm.R;
import com.noobs2d.transportionalarm.adapter.MyCustomAdapter;
import com.noobs2d.transportionalarm.model.AlarmLocationModel;

public class MyCustomAdapterTest extends AndroidTestCase {
	private MyCustomAdapter myCustomAdapter;
	private AlarmLocationModel mAlarmLocationModel1;
	private AlarmLocationModel mAlarmLocationModel2;

	public MyCustomAdapterTest () {
		super();
	}

	protected void setUp () throws Exception {
		super.setUp();
		ArrayList<AlarmLocationModel> alarmLocationModels = new ArrayList<AlarmLocationModel>();
		mAlarmLocationModel1 = new AlarmLocationModel();
		mAlarmLocationModel1.setId(1);
		mAlarmLocationModel1.setAddress("address sample");
		mAlarmLocationModel1.setArea(20.0);
		mAlarmLocationModel1.setLatitude(121.15454);
		mAlarmLocationModel1.setLongitude(14.1454);
		mAlarmLocationModel1.setName("Sample Name");
		mAlarmLocationModel1.setStatus("1");

		mAlarmLocationModel2 = new AlarmLocationModel();
		mAlarmLocationModel2.setId(2);
		mAlarmLocationModel2.setAddress("address sample2");
		mAlarmLocationModel2.setArea(10.0);
		mAlarmLocationModel2.setLatitude(122.15454);
		mAlarmLocationModel2.setLongitude(13.1454);
		mAlarmLocationModel2.setName("Sample Name2");
		mAlarmLocationModel2.setStatus("0");
		alarmLocationModels.add(mAlarmLocationModel1);
		alarmLocationModels.add(mAlarmLocationModel2);
		myCustomAdapter = new MyCustomAdapter(getContext(), R.layout.state_info, alarmLocationModels);

	}

	public void testGetItem1 () {
		assertEquals("mAlarmLocationModel1 was expected.", mAlarmLocationModel1.getId(),
			((AlarmLocationModel)myCustomAdapter.getItem(0)).getId());
		assertEquals("mAlarmLocationModel1 was expected.", mAlarmLocationModel1.getAddress(),
			((AlarmLocationModel)myCustomAdapter.getItem(0)).getAddress());
		assertEquals("mAlarmLocationModel1 was expected.", mAlarmLocationModel1.getArea(),
			((AlarmLocationModel)myCustomAdapter.getItem(0)).getArea());
		assertEquals("mAlarmLocationModel1 was expected.", mAlarmLocationModel1.getLatitude(),
			((AlarmLocationModel)myCustomAdapter.getItem(0)).getLatitude());
		assertEquals("mAlarmLocationModel1 was expected.", mAlarmLocationModel1.getLongitude(),
			((AlarmLocationModel)myCustomAdapter.getItem(0)).getLongitude());
		assertEquals("mAlarmLocationModel1 was expected.", mAlarmLocationModel1.getName(),
			((AlarmLocationModel)myCustomAdapter.getItem(0)).getName());
		assertEquals("mAlarmLocationModel1 was expected.", mAlarmLocationModel1.getStatus(),
			((AlarmLocationModel)myCustomAdapter.getItem(0)).getStatus());
	}

	public void testGetItem2 () {
		assertEquals("mAlarmLocationModel2 was expected.", mAlarmLocationModel2.getId(),
			((AlarmLocationModel)myCustomAdapter.getItem(1)).getId());
		assertEquals("mAlarmLocationModel2 was expected.", mAlarmLocationModel2.getAddress(),
			((AlarmLocationModel)myCustomAdapter.getItem(1)).getAddress());
		assertEquals("mAlarmLocationModel2 was expected.", mAlarmLocationModel2.getArea(),
			((AlarmLocationModel)myCustomAdapter.getItem(1)).getArea());
		assertEquals("mAlarmLocationModel2 was expected.", mAlarmLocationModel2.getLatitude(),
			((AlarmLocationModel)myCustomAdapter.getItem(1)).getLatitude());
		assertEquals("mAlarmLocationModel2 was expected.", mAlarmLocationModel2.getLongitude(),
			((AlarmLocationModel)myCustomAdapter.getItem(1)).getLongitude());
		assertEquals("mAlarmLocationModel2 was expected.", mAlarmLocationModel2.getName(),
			((AlarmLocationModel)myCustomAdapter.getItem(1)).getName());
		assertEquals("mAlarmLocationModel2 was expected.", mAlarmLocationModel2.getStatus(),
			((AlarmLocationModel)myCustomAdapter.getItem(1)).getStatus());
	}

	public void testGetItemId () {
		assertEquals("Wrong ID.", 0, myCustomAdapter.getItemId(0));
	}

	public void testGetCount () {
		assertEquals("Alarm Location Models amount incorrect.", 2, myCustomAdapter.getCount());
	}

	public void testGetView () {
		View convertView = (View)myCustomAdapter.getView(0, null, null);
		TextView alarmLocationName = (TextView)convertView.findViewById(R.id.locationNameTextView);
		TextView alarmLocationAddress = (TextView)convertView.findViewById(R.id.locationAddressTextView);
		TextView alarmLocationLongitude = (TextView)convertView.findViewById(R.id.locationLongitudeTextView);
		TextView alarmLocationLatitude = (TextView)convertView.findViewById(R.id.locationLatitudetextView);
		TextView alarmLocationArea = (TextView)convertView.findViewById(R.id.locationAreaTextView);
		Button alarmLocationEditButtonView = (Button)convertView.findViewById(R.id.editImageView);
		Button alarmLocationRemoveButtonView = (Button)convertView.findViewById(R.id.addLocationImageView);
		CheckBox alarmLocationCheckBox = (CheckBox)convertView.findViewById(R.id.statusCheckBox);

		// On this part you will have to test it with your own views/data
		assertNotNull("ConvertView is null. ", convertView);
		assertNotNull("Address is null.", alarmLocationAddress);
		assertNotNull("Longitude is null.", alarmLocationLongitude);
		assertNotNull("Latitude is null.", alarmLocationLatitude);
		assertNotNull("Area is null.", alarmLocationArea);
		assertNotNull("EditButton is null.", alarmLocationEditButtonView);
		assertNotNull("RemoveButton is null.", alarmLocationRemoveButtonView);
		assertNotNull("CheckBox is null.", alarmLocationCheckBox);
		assertNotNull("Name TextView is null. ", alarmLocationName);

		assertNotSame("View address not expected.", alarmLocationAddress.getText() + "", "");
		assertNotSame("View area not expected.", alarmLocationArea.getText() + "", "");
		assertNotSame("View Latitude not expected.", alarmLocationLatitude.getText() + "", "");
		assertNotSame("View Longitude not expected.", alarmLocationLongitude.getText() + "", "");
		assertNotSame("View Name not expected.", alarmLocationName.getText() + "", "");

		assertNotSame("View Status not expected.", alarmLocationCheckBox.getText() + "", "");

	}
}
