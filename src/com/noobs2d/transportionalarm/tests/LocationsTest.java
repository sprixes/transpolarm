
package com.noobs2d.transportionalarm.tests;

import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.test.suitebuilder.annotation.MediumTest;
import android.text.method.Touch;
import android.widget.Button;
import android.widget.ListView;

import com.noobs2d.transportionalarm.R;
import com.noobs2d.transportionalarm.activity.Locations;
import com.noobs2d.transportionalarm.activity.Map;
import com.noobs2d.transportionalarm.adapter.MyCustomAdapter;

public class LocationsTest extends ActivityInstrumentationTestCase2<Locations> {
	private Locations mLocations;
	private Button mAddLocation;
	private ListView mListview;
	private static final int TIMEOUT_IN_MS = 15000;

	public LocationsTest () {
		super(Locations.class);
	}

	@Override
	protected void setUp () throws Exception {
		super.setUp();

		// Sets the initial touch mode for the Activity under test. This must be called before
		// getActivity()
		setActivityInitialTouchMode(true);
		// Get a reference to the Activity under test, starting it if necessary.
		mLocations = getActivity();
		mAddLocation = (Button)mLocations.findViewById(R.id.addLocationImageView);
		mListview = (ListView)mLocations.findViewById(R.id.listView1);
	}

	@MediumTest
	public void testComponent () {
		assertNotNull("mAddLocation is null", mAddLocation);
		assertNotNull("mListView is null", mListview);
	}

	@MediumTest
	public void testAddLocationButtonEvent () {
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(Map.class.getName(), null, false);
		TouchUtils.clickView(this, mAddLocation);
		Map mapActivity = (Map)activityMonitor.waitForActivityWithTimeout(TIMEOUT_IN_MS);
		// next activity is opened and captured.
		assertNotNull("mapActivity is null", mapActivity);
		assertEquals("Monitor for mapActivity has not been called", 1, activityMonitor.getHits());
		assertEquals("Activity is of wrong type", Map.class, mapActivity.getClass());

		mapActivity.finish();
		getInstrumentation().removeMonitor(activityMonitor);
	}

	@MediumTest
	public void testEditAdapter () {
		MyCustomAdapter myCustomAdapter = (MyCustomAdapter)mListview.getAdapter();
		assertNotNull("MyCustomAdapter is null", myCustomAdapter);
		assertNotNull("MyCustomAdapter Alarm Location Models is null", myCustomAdapter.getAlarmLocationModels());

	}
}
