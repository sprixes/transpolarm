
package com.noobs2d.transportionalarm.tests;

import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.ViewConfiguration;
import android.widget.Button;
import android.widget.TextView;

import com.noobs2d.transportionalarm.R;
import com.noobs2d.transportionalarm.activity.Locations;
import com.noobs2d.transportionalarm.activity.Main;
import com.noobs2d.transportionalarm.controller.ServiceChecker;
import com.noobs2d.transportionalarm.services.LocationCheckServices;

public class MainTest extends ActivityInstrumentationTestCase2<Main> {
	private Main mMain;
	private TextView mDistanceTextView;
	private TextView mNameTextView;
	private Button mServiceButton;
	private Button mDisplayLocation;
	private static final int TIMEOUT_IN_MS = 15000;

	public MainTest () {
		super(Main.class);
	}

	@Override
	protected void setUp () throws Exception {
		super.setUp();
		setActivityInitialTouchMode(true);
		mMain = getActivity();
		mDistanceTextView = (TextView)mMain.findViewById(R.id.distanceTextView);
		mNameTextView = (TextView)mMain.findViewById(R.id.locationNameTextView);
		mServiceButton = (Button)mMain.findViewById(R.id.startServiceButton);
		mDisplayLocation = (Button)mMain.findViewById(R.id.displayLocationId);

	}

	@MediumTest
	public void testComponent () {
		assertNotNull("mDistanceTextView is null", mDistanceTextView);
		assertNotNull("mNameTextView is null", mNameTextView);
		assertNotNull("mServiceButton is null", mServiceButton);
		assertNotNull("mDisplayLocation is null", mDisplayLocation);
	}

	@MediumTest
	public void testDisplayLocationButtonEvent () {
		ActivityMonitor activityMonitor = getInstrumentation().addMonitor(Locations.class.getName(), null, false);
		TouchUtils.clickView(this, mDisplayLocation);
		Locations locationsActivity = (Locations)activityMonitor.waitForActivityWithTimeout(TIMEOUT_IN_MS);
		// next activity is opened and captured.
		assertNotNull("locationsActivity is null", locationsActivity);
		assertEquals("Monitor for locationsActivity has not been called", 1, activityMonitor.getHits());
		assertEquals("Activity is of wrong type", Locations.class, locationsActivity.getClass());

		locationsActivity.finish();
		getInstrumentation().removeMonitor(activityMonitor);
	}

	@MediumTest
	public void testServiceButtonEvent () throws InterruptedException {
		assertEquals(false, ServiceChecker.checkServiceRunning(LocationCheckServices.class, getActivity()));
		TouchUtils.clickView(this, mServiceButton);
		Thread.sleep(ViewConfiguration.get(mMain).getLongPressTimeout());
		assertEquals(true, ServiceChecker.checkServiceRunning(LocationCheckServices.class, getActivity()));
		TouchUtils.clickView(this, mServiceButton);
		Thread.sleep(ViewConfiguration.get(mMain).getLongPressTimeout());
		assertEquals(false, ServiceChecker.checkServiceRunning(LocationCheckServices.class, getActivity()));

	}

}
