
package com.noobs2d.transportionalarm.tests;

import android.graphics.Color;
import android.os.SystemClock;
import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewConfiguration;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.noobs2d.transportionalarm.R;
import com.noobs2d.transportionalarm.activity.Map;
import com.noobs2d.transportionalarm.database.AlarmLocationDataSource;

public class MapTest extends ActivityInstrumentationTestCase2<Map> {
	private Map mMapActivity;
	private EditText mNameEditText;
	private EditText mAreaEditText;
	private GoogleMap mMap;
	private Button mAddLocationButton;
	private AlarmLocationDataSource mAlarmLocationDataSource;
	private Marker mMarker;
	private Circle mCircle;
	private int currentAlarmLocationSize = 0;

	public MapTest () {
		super(Map.class);
	}

	@Override
	protected void setUp () throws Exception {
		super.setUp();

		// Starts the activity under test using the default Intent with:
		// action = {@link Intent#ACTION_MAIN}
		// flags = {@link Intent#FLAG_ACTIVITY_NEW_TASK}
		// All other fields are null or empty.
		mMapActivity = getActivity();
		mNameEditText = (EditText)mMapActivity.findViewById(R.id.nameEditText);
		mAreaEditText = (EditText)mMapActivity.findViewById(R.id.areaEditText);
		mMap = ((MapFragment)mMapActivity.getFragmentManager().findFragmentById(R.id.map)).getMap();
		mAddLocationButton = (Button)mMapActivity.findViewById(R.id.addLocationImageView);
		mAlarmLocationDataSource = new AlarmLocationDataSource(mMapActivity);
	}

	@MediumTest
	public void testComponent () {
		assertNotNull("mNameEditText is null", mNameEditText);
		assertNotNull("mAreaEditText is null", mAreaEditText);
		if (mMapActivity.isGoogleMapsInstalled()) {
			assertNotNull("mMap is null", mMap);
		}
		assertNotNull(mAlarmLocationDataSource);

	}

	@MediumTest
	public void testAddLocation () throws InterruptedException {
		getInstrumentation().runOnMainSync(new Runnable() {
			@Override
			public void run () {
				mNameEditText.requestFocus();
			}
		});
		// Wait until all events from the MainHandler's queue are processed
		getInstrumentation().waitForIdleSync();

		// Send the text message
		getInstrumentation().sendStringSync("Unit Test Name");

		getInstrumentation().waitForIdleSync();

		getInstrumentation().runOnMainSync(new Runnable() {
			@Override
			public void run () {
				mAreaEditText.requestFocus();
			}
		});
		// Wait until all events from the MainHandler's queue are processed
		getInstrumentation().waitForIdleSync();

		// Send the text message
		getInstrumentation().sendStringSync("100");
		getInstrumentation().waitForIdleSync();

		assertEquals("Unit Test Name", mNameEditText.getText().toString());
		assertEquals("10050", mAreaEditText.getText().toString());

		getInstrumentation().waitForIdleSync();

		/*
		 * mAlarmLocationDataSource.open(); currentAlarmLocationSize = mAlarmLocationDataSource.getAllAlarmLocation().size();
		 * mAlarmLocationDataSource.close();
		 */
		TouchUtils.clickView(this, mAddLocationButton);
		/*
		 * mAlarmLocationDataSource.open(); assertNotSame(currentAlarmLocationSize,
		 * mAlarmLocationDataSource.getAllAlarmLocation().size()); mAlarmLocationDataSource.close();
		 */

	}

	public void testLongPress () {
		sendKeyDownUpLong(KeyEvent.KEYCODE_MENU);
	}

	public void sendKeyDownUpLong (final int key) {
		final KeyEvent downEvent = new KeyEvent(KeyEvent.ACTION_DOWN, key);
		sendKeyDownUp(downEvent);
		sleep(500);

		for (int repetition = 0; repetition < 50; repetition++) {
			KeyEvent newEvent = KeyEvent.changeTimeRepeat(downEvent, SystemClock.uptimeMillis(), repetition, downEvent.getFlags()
				| KeyEvent.FLAG_LONG_PRESS);
			sendKeyDownUp(newEvent);
			sleep(5);
		}

		final KeyEvent upEvent = new KeyEvent(KeyEvent.ACTION_UP, key);
		sendKeyDownUp(upEvent);
	}

	public void sendKeyDownUp (KeyEvent key) {
		getInstrumentation().sendKeySync(key);
		getInstrumentation().waitForIdleSync();
	}

	public void sleep (int msec) {
		try {
			Thread.sleep(msec);
		} catch (InterruptedException e) {
			Log.e("nofatclips", "Could not sleep. Mosquito alert!", e);
			return;
		}

	}

}
