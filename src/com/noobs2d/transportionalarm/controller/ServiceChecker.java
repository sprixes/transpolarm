/**
 * 
 */
package com.noobs2d.transportionalarm.controller;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;

/**
 * Copyright (C) 2013 Arjay Paulino 
 * Company <http://www.cybersoft.com> 
 * email<arjay.paulino@cybersoft.ph>
 *
 */
/**
 * @author JR
 * 
 */
public class ServiceChecker {
	public static boolean checkServiceRunning(Class<?> params, Context context) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (params.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
}
