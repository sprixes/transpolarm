
package com.noobs2d.transportionalarm.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.noobs2d.transportionalarm.Constant;
import com.noobs2d.transportionalarm.R;
import com.noobs2d.transportionalarm.controller.ServiceChecker;
import com.noobs2d.transportionalarm.model.AlarmLocationModel;
import com.noobs2d.transportionalarm.services.LocationCheckServices;

public class Main extends Activity {
	public Button serviceButton;
	public TextView distanceTextView;
	public TextView nameTextView;
	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive (Context context, Intent intent) {
			if (intent.getAction().equals(Constant.RECEIVE_LOCATION_ACTION)) {
				Location myLocation = (Location)intent.getExtras().get("location");
				AlarmLocationModel alarmLocationModel = (AlarmLocationModel)intent.getExtras().get("alarmLocationModel");
				Location destination = new Location("");
				destination.setLatitude(alarmLocationModel.getLatitude());
				destination.setLongitude(alarmLocationModel.getLongitude());
				distanceTextView.setText("Distance: " + myLocation.distanceTo(destination) + " meters");
				nameTextView.setText("Name: " + alarmLocationModel.getName());
			}
		}
	};

	@Override
	protected void onResume () {
		IntentFilter filter = new IntentFilter();
		filter.addAction(Constant.RECEIVE_LOCATION_ACTION);
// filter.addAction(Constant.MAIN_MAIN_ACTIVITY_ACTION);
// filter.addAction(Constant.MAIN_CYBERTRACK_SERVICE_ACTION);
// filter.addAction(Constant.MAIN_CYBERTRACK_ENROLLMENT_VERIFICAITON_ACTION);
// filter.addAction(Constant.MAIN_ADD_DOWNLOADED_APPLICATION);
// filter.addAction(Constant.MAIN_ACTION_BOOT_COMPLETED);
		registerReceiver(receiver, filter);
		super.onResume();
	}

	@Override
	protected void onPause () {
		unregisterReceiver(receiver);
		super.onPause();
	}

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainlayout);
		serviceButton = (Button)findViewById(R.id.startServiceButton);
		distanceTextView = (TextView)findViewById(R.id.distanceTextView);
		nameTextView = (TextView)findViewById(R.id.locationNameTextView);
		serviceStatusCheck();

	}

	public void displayLocations (View view) {
		Toast.makeText(this, "displayLocations", Toast.LENGTH_SHORT).show();
		Intent intent = new Intent(this, Locations.class);
		startActivity(intent);
		overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
	}

	public void startTranspolarmService (View view) {
		final LocationManager manager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

		if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			buildAlertMessageNoGps();
		} else {
			Toast.makeText(this, "startTranspolarmService", Toast.LENGTH_SHORT).show();
			if (ServiceChecker.checkServiceRunning(LocationCheckServices.class, this)) {
				stopService(new Intent(this, LocationCheckServices.class));
				serviceButton.setBackgroundResource(R.drawable.playbutton);
			} else {
				startService(new Intent(this, LocationCheckServices.class));
				serviceButton.setBackgroundResource(R.drawable.stopbutton);

			}
		}

	}

	public void serviceStatusCheck () {
		if (ServiceChecker.checkServiceRunning(LocationCheckServices.class, this)) {
			serviceButton.setBackgroundResource(R.drawable.stopbutton);
		} else {
			serviceButton.setBackgroundResource(R.drawable.playbutton);
		}
	}

	@Override
	public void onBackPressed () {
		finish();
		// overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
	}

	private void buildAlertMessageNoGps () {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Your GPS seems to be disabled, do you want to enable it?").setCancelable(false)
			.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				@Override
				public void onClick (@SuppressWarnings("unused") final DialogInterface dialog,
					@SuppressWarnings("unused") final int id) {
					startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
				}
			}).setNegativeButton("No", new DialogInterface.OnClickListener() {
				@Override
				public void onClick (final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
					dialog.cancel();
				}
			});
		final AlertDialog alert = builder.create();
		alert.show();
	}
}
