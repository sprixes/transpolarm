
package com.noobs2d.transportionalarm.activity;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.noobs2d.transportionalarm.R;
import com.noobs2d.transportionalarm.adapter.MyCustomAdapter;
import com.noobs2d.transportionalarm.database.AlarmLocationDataSource;
import com.noobs2d.transportionalarm.interfaces.AdapterListener;
import com.noobs2d.transportionalarm.model.AlarmLocationModel;

public class Locations extends Activity implements AdapterListener {
	private MyCustomAdapter dataAdapter;
	private AlarmLocationDataSource alarmLocationDataSource;
	private List<AlarmLocationModel> alarmLocationModels;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.locationslayout);
		initializeLocationList();
	}

	public void initializeLocationList () {
		// initialize Database
		alarmLocationDataSource = new AlarmLocationDataSource(this);
		alarmLocationDataSource.open();
		alarmLocationModels = alarmLocationDataSource.getAllAlarmLocation();
		alarmLocationDataSource.close();

		// create an ArrayAdaptar from the String Array
		dataAdapter = new MyCustomAdapter(this, R.layout.state_info, alarmLocationModels);
		dataAdapter.setAdapterLister(this);
		ListView listView = (ListView)findViewById(R.id.listView1);

		// Assign adapter to ListView
		listView.setAdapter(dataAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick (AdapterView<?> parent, View view, int position, long id) {
				// When clicked, show a toast with the TextView text
				AlarmLocationModel alarmLocationModel = (AlarmLocationModel)parent.getItemAtPosition(position);
				Toast.makeText(getApplicationContext(), "Clicked on : " + alarmLocationModel.getName(), Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	protected void onResume () {
		initializeLocationList();
		super.onResume();
	}

	public void backTouch (View view) {
		finish();
		overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
	}

	public void addLocation (View view) {

		startActivity(new Intent(this, Map.class));
		overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
	}

	@Override
	public void onBackPressed () {
		finish();
		overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
	}

	@Override
	public void onUpdateAdapter () {
		initializeLocationList();

	}
}
