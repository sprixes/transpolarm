
package com.noobs2d.transportionalarm.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.noobs2d.transportionalarm.R;
import com.noobs2d.transportionalarm.database.AlarmLocationDataSource;
import com.noobs2d.transportionalarm.model.AlarmLocationModel;

public class Map extends Activity {
	private EditText nameEditText;
	private EditText areaEditText;
	private GoogleMap map;
	private AlarmLocationDataSource alarmLocationDataSource;

	protected Marker marker;
	protected Circle circle;
	private AlarmLocationModel alarmLocationModelEdit = null;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.maplayout);
		setUpMapIfNeeded();
	}

	public void setUpMap () {
		nameEditText = (EditText)findViewById(R.id.nameEditText);
		areaEditText = (EditText)findViewById(R.id.areaEditText);
		areaEditText.setText("50");
		alarmLocationDataSource = new AlarmLocationDataSource(this);
		// Get a handle to the Map Fragment
		map = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();

		LatLng paseoLonglat = new LatLng(14.556802, 121.021145);
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(paseoLonglat, 15));

		if (getIntent().hasExtra("AlarmLocationModel")) {
			alarmLocationModelEdit = (AlarmLocationModel)getIntent().getSerializableExtra("AlarmLocationModel");
			LatLng point = new LatLng(alarmLocationModelEdit.getLatitude(), alarmLocationModelEdit.getLongitude());
			marker = map.addMarker(new MarkerOptions().title(nameEditText.getText().toString()).position(point));
			CircleOptions circleOptions = new CircleOptions();
			circleOptions.center(point);
			circleOptions.radius(alarmLocationModelEdit.getArea());

			circleOptions.strokeWidth(2);
			circleOptions.strokeColor(Color.BLUE);
			circleOptions.fillColor(Color.parseColor("#500084d3"));
			circleOptions.fillColor(Color.BLUE);
			circle = map.addCircle(circleOptions);
			map.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 15));
			nameEditText.setText(alarmLocationModelEdit.getName());
			areaEditText.setText(alarmLocationModelEdit.getArea() + "");
		}

		map.setOnMapLongClickListener(new OnMapLongClickListener() {

			@Override
			public void onMapLongClick (LatLng point) {

				if (marker != null) {
					marker.remove();
					circle.remove();
				}
				marker = map.addMarker(new MarkerOptions().title(nameEditText.getText().toString()).position(point));
				CircleOptions circleOptions = new CircleOptions();
				circleOptions.center(point);
				if (areaEditText.getText().toString().matches("")) {
					circleOptions.radius(50);
					areaEditText.setText("50");
				} else {
					circleOptions.radius(Double.valueOf(areaEditText.getText().toString()));
				}
				circleOptions.strokeWidth(2);
				circleOptions.strokeColor(Color.BLUE);
				circleOptions.fillColor(Color.parseColor("#500084d3"));
				circleOptions.fillColor(Color.BLUE);
				circle = map.addCircle(circleOptions);
			}
		});
	}

	private void setUpMapIfNeeded () {
		// Do a null check to confirm that we have not already instantiated the map.
		if (map == null) {
			// Try to obtain the map from the SupportMapFragment.
			map = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();

			if (isGoogleMapsInstalled()) {
				if (map != null) {
					setUpMap();
				}
			} else {
				Builder builder = new AlertDialog.Builder(this);
				builder.setMessage("Install Google Maps");
				builder.setCancelable(false);
				builder.setPositiveButton("Install", getGoogleMapsListener());
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		}
	}

	public boolean isGoogleMapsInstalled () {
		try {
			ApplicationInfo info = getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}

	public OnClickListener getGoogleMapsListener () {
		return new OnClickListener() {
			@Override
			public void onClick (DialogInterface dialog, int which) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.apps.maps"));
				startActivity(intent);

				// Finish the activity so they can't circumvent the check
				finish();
			}
		};
	}

	public void backTouch (View view) {
		finish();
		overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
	}

	public void saveLocation (View view) {
		if (marker != null && !isEmpty(nameEditText) && !isEmpty(areaEditText)) {
			AlarmLocationModel alarmLocationModel = new AlarmLocationModel();

			alarmLocationModel.setName(nameEditText.getText().toString());
			alarmLocationModel.setArea(Double.valueOf(areaEditText.getText().toString()));
			alarmLocationModel.setAddress("no address yet/Geocode");
			alarmLocationModel.setLatitude(marker.getPosition().latitude);
			alarmLocationModel.setLongitude(marker.getPosition().longitude);
			alarmLocationModel.setStatus("1");
			Toast.makeText(this, alarmLocationModel.toString(), Toast.LENGTH_LONG).show();
			alarmLocationDataSource.open();
			alarmLocationDataSource.updateAllAlarmLocation();
			if (alarmLocationModelEdit != null) {
				alarmLocationModel.setId(alarmLocationModelEdit.getId());
				alarmLocationDataSource.updateAlarmLocation(alarmLocationModel);
			} else {
				alarmLocationDataSource.addAlarmLocation(alarmLocationModel);
			}

			alarmLocationDataSource.close();
			finish();
			overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
		} else {
			Toast.makeText(this, "Add Location to Save", Toast.LENGTH_LONG).show();
		}

	}

	private boolean isEmpty (EditText etText) {
		return etText.getText().toString().trim().length() == 0;
	}

	@Override
	public void onBackPressed () {
		finish();
		overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
	}

}
