
package com.noobs2d.transportionalarm.activity;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.noobs2d.transportionalarm.R;
import com.noobs2d.transportionalarm.services.LocationCheckServices;

public class Alarm extends Activity {
	Vibrator v;
	long[] pattern = {0, 100, 500, 100, 500, 100, 500, 100, 500, 100, 500};
	NotificationManager mNotificationManager;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.alarmlayout);
		v = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
		// Vibrate for 500 milliseconds
		v.vibrate(99999);
		android.support.v4.app.NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
		mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		mBuilder.setSmallIcon(R.drawable.logo);
		mBuilder.setContentTitle("Alarm");

		mBuilder.setOngoing(true);

		// Creates an explicit intent for an Activity in your app

		// mId allows you to update the notification later on.
		mNotificationManager.notify(1, mBuilder.build());

		PowerManager powerManager = (PowerManager)getSystemService(POWER_SERVICE);
		WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakelockTag");
		wakeLock.acquire();
		final Window win = getWindow();
		win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
		win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
	}

	public void stopAlarm (View view) {
		mNotificationManager.cancelAll();
		stopService(new Intent(this, LocationCheckServices.class));
		v.cancel();
		finish();
	}
}
