/**
 * 
 */
package com.noobs2d.transportionalarm.locationUtils;

import android.location.Location;

/**
 * Copyright (C) 2013 Arjay Paulino 
 * Company <http://www.cybersoft.com> 
 * email<arjay.paulino@cybersoft.ph>
 *
 */
/**
 * @author JR
 * 
 */
public interface MyCurrentLocationListener {

	public void updateLocation(Location location);

}
