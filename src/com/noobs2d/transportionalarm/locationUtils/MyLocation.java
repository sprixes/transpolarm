
package com.noobs2d.transportionalarm.locationUtils;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

public class MyLocation implements LocationListener, GooglePlayServicesClient.ConnectionCallbacks,
	GooglePlayServicesClient.OnConnectionFailedListener {
	public static final String DEBUG_TAG = MyLocation.class.getSimpleName();
// A request to connect to Location Services
	private LocationRequest mLocationRequest;

// Stores the current instantiation of the location client in this object
	private LocationClient mLocationClient;
// Handle to SharedPreferences for this app
	SharedPreferences mPrefs;

// Handle to a SharedPreferences editor
	SharedPreferences.Editor mEditor;
	/** main context **/
	private Context context;

	LocationListener locationListener;

	/** MyLocation Constructor setting up context **/
	public MyLocation (Context context) {
		Log.d(DEBUG_TAG, "MyLocation");
		this.context = context;
		mLocationRequest = LocationRequest.create();

		/*
		 * Set the update interval
		 */
		mLocationRequest.setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS);

		// Use high accuracy
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

		// Set the interval ceiling to one minute
		mLocationRequest.setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS);

		// Note that location updates are off until the user turns them on

		// Open Shared Preferences
		mPrefs = context.getSharedPreferences(LocationUtils.SHARED_PREFERENCES, Context.MODE_PRIVATE);

		// Get an editor
		mEditor = mPrefs.edit();

		/*
		 * Create a new location client, using the enclosing class to handle callbacks.
		 */
		mLocationClient = new LocationClient(context, this, this);
		mLocationClient.connect();
		startUpdates();
	}

	public void setLocationListener (LocationListener locationListener) {
		this.locationListener = locationListener;
	}

	public static boolean servicesConnected (Context context) {
		Log.d(DEBUG_TAG, "servicesConnected");
		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);

		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// In debug mode, log the status
			// Log.d(LocationUtils.APPTAG, getString(R.string.play_services_available));

			// Continue
			Log.d(DEBUG_TAG, "servicesConnected# true");
			return true;
			// Google Play services was not available for some reason
		} else {
			Log.d(DEBUG_TAG, "servicesConnected# false");
			Toast.makeText(context, "Google Play services was not available for some reason", Toast.LENGTH_LONG).show();
			return false;
		}
	}

	public void startUpdates () {
		Log.d(DEBUG_TAG, "startUpdates");
		if (servicesConnected(context)) {
			// startPeriodicUpdates();
		}
	}

	/** Invoked by the "Stop Updates" button Sends a request to remove location updates request them.
	 * 
	 * @param v The view object associated with this method, in this case a Button. */
	public void stopUpdates () {
		Log.d(DEBUG_TAG, "stopUpdates");
		if (servicesConnected(context)) {
			stopPeriodicUpdates();
		}
	}

	public Location getCurrentLocation () {

		// If Google Play Services is available
		if (servicesConnected(context)) {

			// Get the current location
			// Location currentLocation = mLocationClient.getLastLocation();

			// Display the current location in the UI
			return mLocationClient.getLastLocation();
		}
		return null;
	}

	public void onDestroy () {
		Log.d(DEBUG_TAG, "onDestroy");
		// If the client is connected
		if (mLocationClient.isConnected()) {
			stopPeriodicUpdates();
		}

		// After disconnect() is called, the client is considered "dead".
		mLocationClient.disconnect();

	}

	/** In response to a request to start updates, send a request to Location Services */
	private void startPeriodicUpdates () {

		mLocationClient.requestLocationUpdates(mLocationRequest, this);

	}

	/** In response to a request to stop updates, send a request to Location Services */
	private void stopPeriodicUpdates () {
		mLocationClient.removeLocationUpdates(this);

	}

	@Override
	public void onConnectionFailed (ConnectionResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnected (Bundle arg0) {
		startPeriodicUpdates();

	}

	@Override
	public void onDisconnected () {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationChanged (Location arg0) {
		if (locationListener != null) {
			locationListener.onLocationChanged(arg0);
		}
	}

}
