
package com.noobs2d.transportionalarm.model;

import java.io.Serializable;

public class AlarmLocationModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private double longitude;
	private double latitude;
	private String Name;
	private String Address;
	private double Area;
	private String Status;

	public int getId () {
		return id;
	}

	public void setId (int id) {
		this.id = id;
	}

	public double getLongitude () {
		return longitude;
	}

	public void setLongitude (double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude () {
		return latitude;
	}

	public void setLatitude (double latitude) {
		this.latitude = latitude;
	}

	public String getName () {
		return Name;
	}

	public void setName (String name) {
		Name = name;
	}

	public String getAddress () {
		return Address;
	}

	public void setAddress (String address) {
		Address = address;
	}

	public double getArea () {
		return Area;
	}

	public void setArea (double area) {
		Area = area;
	}

	public String getStatus () {
		return Status;
	}

	public void setStatus (String status) {
		Status = status;
	}

	@Override
	public String toString () {
		return "AlarmLocationModel [id=" + id + ", longitude=" + longitude + ", latitude=" + latitude + ", Name=" + Name
			+ ", Address=" + Address + ", Area=" + Area + "]";
	}

}
