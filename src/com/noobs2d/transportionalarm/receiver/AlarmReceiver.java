
package com.noobs2d.transportionalarm.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.noobs2d.transportionalarm.Constant;
import com.noobs2d.transportionalarm.activity.Alarm;
import com.noobs2d.transportionalarm.model.AlarmLocationModel;

public class AlarmReceiver extends BroadcastReceiver {
	AlarmLocationModel alarmLocationModel;

	@Override
	public void onReceive (Context context, Intent intent) {
		if (intent.getAction().equals(Constant.RECEIVE_ALARM_ACTION)) {
			Log.d("logs", "ALARM");

			alarmLocationModel = (AlarmLocationModel)intent.getSerializableExtra("alarmModel");

			Intent alarmIntent = new Intent(context, Alarm.class);
			alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			//alarmIntent.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED + WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD+ WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON + WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
			context.startActivity(alarmIntent);
		}
	}

}
