
package com.noobs2d.transportionalarm.database;

import java.util.ArrayList;
import java.util.List;

import com.noobs2d.transportionalarm.model.AlarmLocationModel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class AlarmLocationDataSource {
	public static final String DEBUG_TAG = AlarmLocationDataSource.class.getCanonicalName();
	// Database fields
	private SQLiteDatabase database;
	private MySQLiteHelper dbHelper;

	public AlarmLocationDataSource (Context context) {
		dbHelper = new MySQLiteHelper(context);
	}

	public void open () throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close () {
		dbHelper.close();
	}

	public void addAlarmLocation (AlarmLocationModel alarmLocationModel) {
		ContentValues values = new ContentValues();

		values.put(MySQLiteHelper.LONGITUDE_FIELD, alarmLocationModel.getLongitude());
		values.put(MySQLiteHelper.LATITUDE_FIELD, alarmLocationModel.getLatitude());
		values.put(MySQLiteHelper.NAME_FIELD, alarmLocationModel.getName());
		values.put(MySQLiteHelper.DESCRIPTION_FIELD, alarmLocationModel.getAddress());
		values.put(MySQLiteHelper.AREA_FIELD, alarmLocationModel.getArea());
		values.put(MySQLiteHelper.STATUS_FIELD, alarmLocationModel.getStatus());

		database.insert(MySQLiteHelper.TABLE_ALARM_LOCATION, null, values);
	}

	public void updateAlarmLocation (AlarmLocationModel alarmLocationModel) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.LONGITUDE_FIELD, alarmLocationModel.getLongitude());
		values.put(MySQLiteHelper.LATITUDE_FIELD, alarmLocationModel.getLatitude());
		values.put(MySQLiteHelper.NAME_FIELD, alarmLocationModel.getName());
		values.put(MySQLiteHelper.DESCRIPTION_FIELD, alarmLocationModel.getAddress());
		values.put(MySQLiteHelper.AREA_FIELD, alarmLocationModel.getArea());
		values.put(MySQLiteHelper.STATUS_FIELD, alarmLocationModel.getStatus());

		database.update(MySQLiteHelper.TABLE_ALARM_LOCATION, values, MySQLiteHelper.TABLE_ID + "=" + alarmLocationModel.getId(),
			null);
	}

	public void updateAllAlarmLocation () {
		ContentValues values = new ContentValues();

		values.put(MySQLiteHelper.STATUS_FIELD, "0");

		database.update(MySQLiteHelper.TABLE_ALARM_LOCATION, values, null, null);
	}

	public void truncateAlarmLocation () {
		Log.d(DEBUG_TAG, "deleting " + MySQLiteHelper.TABLE_ALARM_LOCATION);
		try {
			database.execSQL("delete from " + MySQLiteHelper.TABLE_ALARM_LOCATION);
		} catch (SQLException e) {
			Log.d(DEBUG_TAG, MySQLiteHelper.TABLE_ALARM_LOCATION + " error:" + e);
		}

		Log.d(DEBUG_TAG, "deleted " + MySQLiteHelper.TABLE_ALARM_LOCATION);
	}

	public void deleteAlarmLocation (AlarmLocationModel alarmLocationModel) {
		long id = alarmLocationModel.getId();
		database.delete(MySQLiteHelper.TABLE_ALARM_LOCATION, MySQLiteHelper.TABLE_ID + "=" + id, null);
	}

	public List<AlarmLocationModel> getAllAlarmLocation () {
		List<AlarmLocationModel> alarmLocationModels = new ArrayList<AlarmLocationModel>();

		Cursor cursor = database.query(MySQLiteHelper.TABLE_ALARM_LOCATION, null, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			AlarmLocationModel alarmLocationModel = cursorToAlarmLocationModel(cursor);
			alarmLocationModels.add(alarmLocationModel);
			cursor.moveToNext();
		}
		cursor.close();
		return alarmLocationModels;
	}

	public AlarmLocationModel cursorToAlarmLocationModel (Cursor cursor) {
		AlarmLocationModel alarmLocationModel = new AlarmLocationModel();
		alarmLocationModel.setId(cursor.getInt(0));
		alarmLocationModel.setLongitude(cursor.getFloat(1));
		alarmLocationModel.setLatitude(cursor.getFloat(2));
		alarmLocationModel.setName(cursor.getString(3));
		alarmLocationModel.setAddress(cursor.getString(4));
		alarmLocationModel.setArea(cursor.getFloat(5));
		alarmLocationModel.setStatus(cursor.getString(6));
		return alarmLocationModel;
	}

	public AlarmLocationModel getAlarmLocation (int id) {
		String selectQuery = "SELECT  *  FROM " + MySQLiteHelper.TABLE_ALARM_LOCATION + " where " + MySQLiteHelper.TABLE_ID + "='"
			+ id + "'";
		Cursor cursor = database.rawQuery(selectQuery, null);
		AlarmLocationModel alarmLocationModel = null;
		try {
			if (cursor.moveToFirst()) {
				alarmLocationModel = cursorToAlarmLocationModel(cursor);
			}
		} finally {
			cursor.close();
		}

		return alarmLocationModel;
	}
}
