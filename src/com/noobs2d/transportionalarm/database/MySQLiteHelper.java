/**
 * 
 */

package com.noobs2d.transportionalarm.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Copyright (C) 2013 Arjay Paulino 
 * Company <http://www.cybersoft.com> 
 * email<arjay.paulino@cybersoft.ph>
 *
 */
/** @author JR */
public class MySQLiteHelper extends SQLiteOpenHelper {

	public static final String TABLE_ALARM_LOCATION = "questiontable";

	private static final String DATABASE_NAME = "alarmlocationdatabase";
	private static final int DATABASE_VERSION = 3;

	public static final String TABLE_ID = "_id";
	public static final String LONGITUDE_FIELD = "Longitude";
	public static final String LATITUDE_FIELD = "Latitude";
	public static final String NAME_FIELD = "Name";
	public static final String DESCRIPTION_FIELD = "Description";
	public static final String AREA_FIELD = "Area";
	public static final String STATUS_FIELD = "Status";

	public MySQLiteHelper (Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate (SQLiteDatabase database) {
		createAlarmLocationTable(database);

	}

	public void createAlarmLocationTable (SQLiteDatabase database) {
		String executeQuery = "";
		executeQuery += TABLE_ID + " INTEGER PRIMARY KEY,";
		executeQuery += LONGITUDE_FIELD + " TEXT,";
		executeQuery += LATITUDE_FIELD + " TEXT,";
		executeQuery += NAME_FIELD + " TEXT,";
		executeQuery += DESCRIPTION_FIELD + " TEXT,";
		executeQuery += AREA_FIELD + " TEXT,";
		executeQuery += STATUS_FIELD + " TEXT";

		String CREATE_QUESTION_TABLE = "CREATE TABLE " + TABLE_ALARM_LOCATION + "(" + executeQuery + ")";
		database.execSQL(CREATE_QUESTION_TABLE);

	}

	@Override
	public void onUpgrade (SQLiteDatabase database, int oldVersion, int newVersion) {
		Log.w(MySQLiteHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion
			+ ", which will destroy all old data");
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_ALARM_LOCATION);

		onCreate(database);
	}

}
