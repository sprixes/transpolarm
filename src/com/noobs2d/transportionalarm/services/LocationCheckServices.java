
package com.noobs2d.transportionalarm.services;

import java.util.List;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.google.android.gms.location.LocationListener;
import com.noobs2d.transportionalarm.Constant;
import com.noobs2d.transportionalarm.R;
import com.noobs2d.transportionalarm.activity.Main;
import com.noobs2d.transportionalarm.database.AlarmLocationDataSource;
import com.noobs2d.transportionalarm.locationUtils.MyLocation;
import com.noobs2d.transportionalarm.model.AlarmLocationModel;

public class LocationCheckServices extends Service implements LocationListener {
	private MyLocation myLocation;
	private AlarmLocationDataSource alarmLocationDataSource;
	private List<AlarmLocationModel> alarmLocationModels;
	NotificationCompat.Builder mBuilder;
	NotificationManager mNotificationManager;

	@Override
	public IBinder onBind (Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate () {
		myLocation = new MyLocation(this);
		alarmLocationDataSource = new AlarmLocationDataSource(getApplicationContext());
		super.onCreate();
	}

	@Override
	public int onStartCommand (Intent intent, int flags, int startId) {
		showNotification();
		myLocation.setLocationListener(this);
		alarmLocationDataSource.open();
		alarmLocationModels = alarmLocationDataSource.getAllAlarmLocation();
		alarmLocationDataSource.close();
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy () {
		myLocation.onDestroy();
		hideNotification();
		super.onDestroy();
	}

	@Override
	public void onLocationChanged (Location location) {

		for (int alarmLocationIndex = 0; alarmLocationIndex < alarmLocationModels.size(); alarmLocationIndex++) {

			Location destination = new Location("");
			destination.setLatitude(alarmLocationModels.get(alarmLocationIndex).getLatitude());
			destination.setLongitude(alarmLocationModels.get(alarmLocationIndex).getLongitude());
			if (alarmLocationModels.get(alarmLocationIndex).getStatus().equals("1")) {
				Intent intentMain = new Intent();
				intentMain.setAction(Constant.RECEIVE_LOCATION_ACTION);
				intentMain.putExtra("location", location);
				intentMain.putExtra("alarmLocationModel", alarmLocationModels.get(alarmLocationIndex));
				sendBroadcast(intentMain);
				if (location.distanceTo(destination) < alarmLocationModels.get(alarmLocationIndex).getArea()) {
					Intent intent = new Intent();
					intent.setAction(Constant.RECEIVE_ALARM_ACTION);
					alarmLocationModels.get(alarmLocationIndex).setStatus("0");
					alarmLocationDataSource.open();
					alarmLocationDataSource.updateAlarmLocation(alarmLocationModels.get(alarmLocationIndex));
					alarmLocationDataSource.close();
					intent.putExtra("alarmModel", alarmLocationModels.get(alarmLocationIndex));
					sendBroadcast(intent);

				}
			}
		}

		// check if activity is showing

	}

	public void showNotification () {
		mBuilder = new NotificationCompat.Builder(this);
		mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		mBuilder.setSmallIcon(R.drawable.logo);
		mBuilder.setContentTitle("Transpolarm Service Running");
		mBuilder.setOngoing(true);

		// Creates an explicit intent for an Activity in your app
		Intent resultIntent = new Intent(this, Main.class);

		/*
		 * The stack builder object will contain an artificial back stack for the started Activity. This ensures that navigating
		 * backward from the Activity leads out of your application to the Home screen.
		 */
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(Main.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.setContentIntent(resultPendingIntent);

		// mId allows you to update the notification later on.
		mNotificationManager.notify(1, mBuilder.build());
	}

	public void hideNotification () {
		mNotificationManager.cancel(1);
	}

}
