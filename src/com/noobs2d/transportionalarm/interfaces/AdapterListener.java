
package com.noobs2d.transportionalarm.interfaces;

public interface AdapterListener {
	void onUpdateAdapter ();
}
